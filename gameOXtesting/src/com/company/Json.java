package com.company;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by ^_^ on 15.03.2018.
 */
public class Json extends HttpUrlConnect {

    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    public void JsonConnect() {
        Person person = new Person("ru.rgs.life.rgs_mka", "1.0", 1455805089, "http:\\/\\/bnet.i-partner.ru\\/l\\/h3Ydb\\/", "https:\\/\\/bnet.i-partner.ru\\/l\\/files\\/0618f2ff3d8389db369f277bf642b612", "");
//    получим строковое представление в Json фрмате от нашего объекта
        String json = GSON.toJson(person);
        System.out.println(json);

//        Десирализация с методом fromJson, где 1 параметр - полученный Json
//        2 параметр имя класса в соответсвии которого нужно произвести восстановление json в java

        Person person1 = GSON.fromJson(json, Person.class);
        System.out.println(person1.getName());
        System.out.println(person1.getVersion());
        System.out.println(person1.getTime());
        System.out.println(person1.getUrl());
        System.out.println(person1.getFile());
        System.out.println(person1.getComment());
//        System.out.println(person1.getGeoHistory());

    }
}

    class Person {
        private String name;
        private String version;
        private int time;
        private String url;
        private String file;
        private String comment;

//        @SerializedName("geo")
//        private List<String> geoHistory = new ArrayList<>();
        //методы для доступа к полям


        public String getVersion() {
            return version;
        }

        public String getUrl() {
            return url;
        }

        public String getFile() {
            return file;
        }

        public String getComment() {
            return comment;
        }

        public int getTime() {
            return time;
        }

        public String getName() {
            return name;
        }
//List<String> geoHistory
        public Person (String name, String version, int time, String url, String file, String comment) {
            this.name = name;
            this.version = version;
            this.time = time;
            this.url = url;
            this.file = file;
            this.comment = comment;
//            this.geoHistory = geoHistory;
        }
    }