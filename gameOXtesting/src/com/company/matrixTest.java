package com.company;

import java.util.Scanner;

import static com.sun.xml.internal.bind.v2.schemagen.Util.equal;

/**
 * Created by ^_^ on 12.03.2018.
 */
public class MatrixTest {

    private String[][] matrix;
    private int count = 0;
    private String nameX;
    private String nameO;

    public int setSizeMatrix(int a){
        matrix = new String[a][a];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                matrix[i][j] = "[ ]";
            }
        }
        printMatrix(a);
        return a;
    }

    private void printMatrix(int a) {

        System.out.println("   [1] [2] [3]");

        for (int i = 0; i < a; i++) {
            System.out.print("[" + (i + 1) + "]");
            for (int j = 0; j < a; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        
       changePlayers(nameX, nameO);
    }

    private void setmMtrixA(int i, int j) {
        if (matrix[i - 1][j - 1] == "[ ]") {
            matrix[i - 1][j - 1] = "[" + (count % 2 == 0 ? "x" : "o") + "]";
            count++;
        } else {
            System.out.println("Ячейка занята");
        }
    }

    public void inputName(){
        Scanner in = new Scanner(System.in);
        System.out.print("Введите имя игрока [X] ");
        nameX = in.next();
        System.out.print("Введите имя игрока [O] ");
        nameO = in.next();
    }

    private void changePlayers (String nameX, String nameO) {
        if (count % 2 == 0) {
            System.out.println("\nХодит " + nameX);
        } else {
            System.out.println("\nХодит " + nameO);
        }
    }
    
    private boolean checkLine(String X) {
        if (equal(matrix[0][0], X) && equal(matrix[0][1], X) && equal(matrix[0][2], X)) return true;
        if (equal(matrix[1][0], X) && equal(matrix[1][1], X) && equal(matrix[1][2], X)) return true;
        if (equal(matrix[2][0], X) && equal(matrix[2][1], X) && equal(matrix[2][2], X)) return true;
        if (equal(matrix[0][0], X) && equal(matrix[1][1], X) && equal(matrix[2][2], X)) return true;
        if (equal(matrix[0][2], X) && equal(matrix[1][1], X) && equal(matrix[2][0], X)) return true;
        if (equal(matrix[0][0], X) && equal(matrix[1][0], X) && equal(matrix[2][0], X)) return true;
        if (equal(matrix[0][1], X) && equal(matrix[1][1], X) && equal(matrix[2][1], X)) return true;
        if (equal(matrix[0][2], X) && equal(matrix[1][2], X) && equal(matrix[2][2], X)) return true;
        return false;
    }

    public void logicGame(int a){

        int i = 0, j = 0;

        Scanner in = new Scanner(System.in);
        String x = "[x]";
        String o = "[o]";

        while (count < 3*a) {

            do {
                System.out.print("Введите координаты: ");
                j = in.nextInt();
                i = in.nextInt();
                if(i < 0 || i > a || j < 0 || j > a){
                    System.out.print("Ошибка! Необходимо ввести координаты от 1 до 3: ");
                }}while (i < 0 || i > a || j < 0 || j > a);

            setmMtrixA(i,j);

            printMatrix(a);

            if (checkLine(x)) {
                System.out.println("Молодец, выйграл " + nameX);
                break;
            }
            if (checkLine(o)) {
                System.out.println("Молодец, выйграл " + nameO);
                break;
            }
        }
        if ((!checkLine(x)) & (!checkLine(o))) {
            System.out.println("Ничья");
        }

    }
}
