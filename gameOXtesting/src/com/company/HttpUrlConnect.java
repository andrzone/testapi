package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ^_^ on 15.03.2018.
 */
public class HttpUrlConnect {

    public void url() throws IOException {
        String query = "http://bnet.i-partner.ru/projects/playMarket/?json=1";

        HttpURLConnection connection = null; // пока в нолевом
        //блок с основной логикой
        try {
            //где URL - в качестве параметра своего конструктора принимает строковое значение адреса
            //по которому нужно обратиться
            connection = (HttpURLConnection) new URL(query).openConnection(); //теперь есть соединение
            //настроим соединение
            connection.setRequestMethod("GET"); //можно не писать, GET по умолчанию
            connection.setUseCaches(false);//не использовать кэширование запроса
            connection.setConnectTimeout(250);//установим время подключение
            connection.setReadTimeout(250);//установим время чтения от сервера

            //управляем подключением
            connection.connect();

            //Общий результат

            StringBuilder sb = new StringBuilder();

            //проанализируем ответ
            //если код ответа 200(HTTP_OK), то запрос успешный, можно читать ответ
            if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
                //если запрос успешный, объявим BufferedReader, который позволяет вычитывать объем данных
                //BufferedReader принимает в качестве параметра конструктора StreamReader
                // и передаем InputStream получаемый из объекта connection
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "cp1251"));
                //выполнени чтения из входного потока в цикле
                String line;
                //строке присваиваем значение вычитанной строки и проверка строка не нулевая
                //и строку необходимо инициализировать к общему результату sb.
                while ((line = in.readLine()) != null) {
                    sb.append(line);//добавляем к общему результату строку
                    sb.append("\n");
                }
                System.out.println(sb.toString());

                //если запрос завершился с кодом отличным от 200, то сообщение об ошибке
                //
            } else {
                System.out.println("fail: " + connection.getResponseCode() + "," + connection.getResponseMessage());
            }

            //вводим ошибки
        } catch (Throwable cause) {
            cause.printStackTrace();
            //независимо от того, была ошибка или нет
            // = проверка на то, было ли создано подключение и закроем подключение
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
