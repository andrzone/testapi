package com.example.android.apinext;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ^_^ on 16.03.2018.
 */

public class JsonAdapter extends ArrayAdapter<Json> {
    List<Json> jsonList;
    Context context;
    private LayoutInflater mInflater;

    public JsonAdapter(@NonNull Context context, int resource, @NonNull List<Json> objects, List<Json> jsonList, Context context1, LayoutInflater mInflater) {
        super(context, resource, objects);
        this.jsonList = jsonList;
        this.context = context1;
        this.mInflater = mInflater;
    }

    @Override
    public Json getItem(int position) {
        return jsonList.get(position);
    }

    private static class ViewHolder {
        private final RelativeLayout rootView;
        private final ImageView imageView;
        private final TextView textViewName;
        private final TextView textViewE;


        private ViewHolder(RelativeLayout rootView, ImageView imageView, TextView textViewName, TextView textViewE) {
            this.rootView = rootView;
            this.imageView = imageView;
            this.textViewName = textViewName;
            this.textViewE = textViewE;
        }

        private static ViewHolder create(RelativeLayout rootView) {
            ImageView imageView = (ImageView) rootView.findViewById(R.id.imageView);
            TextView textViewName = (TextView) rootView.findViewById(R.id.textViewName);
            TextView textViewE = (TextView)  rootView.findViewById(R.id.textViewE);
            return new ViewHolder(rootView, imageView, textViewName,textViewE);
        }
    }
}