package com.example.android.apinext;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ^_^ on 16.03.2018.
 */

public class JsonList {

    @SerializedName("jsons")
    @Expose
    private ArrayList<Json> jsons = new ArrayList<>();

    public ArrayList<Json> getJsons() { return  jsons; }

    public void  setJsons(ArrayList<Json> jsons) { this.jsons = jsons; }


}
