package com.example.android.apinext;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ^_^ on 13.03.2018.
 */

public class StartApp {

    public String StartApp() throws IOException {
        String url = "http://bnet.i-partner.ru/projects/playMarket/?json=1";
        URL obj;
        obj = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

        connection.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        String x = response.toString();
        return x;
    }
}
