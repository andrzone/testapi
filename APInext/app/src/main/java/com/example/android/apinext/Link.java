package com.example.android.apinext;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by ^_^ on 16.03.2018.
 */

public interface Link {
    @GET("/var/www/bnet/projects/playMarket")
    Call<JsonList> getMyJSON();
}