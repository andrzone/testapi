package com.example.android.playmarket;

import android.app.Application;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ^_^ on 19.03.2018.
 */

public class RetrofitGason extends Application {
    private static RetrofitInterface retrofitInterface;
    private Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://bnet.i-partner.ru/projects/playMarket/?json=1")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        retrofitInterface = retrofit.create(RetrofitInterface.class);
    }

    public static RetrofitInterface getApi(){
        return retrofitInterface;
    }

}


