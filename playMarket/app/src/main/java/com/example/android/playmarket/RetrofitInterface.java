package com.example.android.playmarket;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by ^_^ on 19.03.2018.
 */

public interface RetrofitInterface {
    @GET("/projects/playMarket/?json=1")
    Call<List<PostModel>> getData();
}


