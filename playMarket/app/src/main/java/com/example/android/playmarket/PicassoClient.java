package com.example.android.playmarket;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by ^_^ on 22.03.2018.
 */

public class PicassoClient {

    public static void dowImage(Context context, ImageView img, String url) {

        if(url != null && url.length()>0)
        {
            Picasso.with(context).load(url).resize(150,150).placeholder(R.drawable.ic_launcher_background).into(img);

        }else {
            Picasso.with(context).load(R.drawable.ic_launcher_background).resize(250,250).into(img);
        }
    }



}
