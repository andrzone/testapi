package com.example.android.playmarket;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ^_^ on 19.03.2018.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    private List<PostModel> posts;
    private ArrayList<ImageList> imageLists;
    Context context;

    public RecyclerAdapter(Context context, List<PostModel> posts, ArrayList<ImageList> imageLists) {
        this.context = context;
        this.posts = posts;
        this.imageLists = imageLists;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final PostModel post = posts.get(position);
        PicassoClient.dowImage(context, holder.image,imageLists.get(position).getUrl());
        holder.post.setText(Html.fromHtml(post.getName()));
        holder.time.setText(post.getTime());
        holder.version.setText(post.getVersion());
        String file = post.getFile();
        MainActivity mainActivity = new MainActivity();
        mainActivity.perEmen(file);
        }

    @Override
    public int getItemCount() {
        if (posts == null)
            return 0;
        return posts.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView post;
        TextView time;
        TextView version;
        ImageButton file;
        ImageButton image;

        public ViewHolder(View itemView) {
            super(itemView);
            post = (TextView) itemView.findViewById(R.id.postitem_post);
            time = (TextView) itemView.findViewById(R.id.postitem_time);
            version = (TextView) itemView.findViewById(R.id.postitem_version);
            file = (ImageButton) itemView.findViewById(R.id.postitem_image);
            image = (ImageButton) itemView.findViewById(R.id.postitem_image);

        }
    }

    public void Context (Context context) {
        this.context = context;
    }
}
