package com.example.android.playmarket;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class post_itemTwo extends AppCompatActivity {
    MainActivity mainActivity = new MainActivity();
    private String file = mainActivity.files();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_item_two);
    }

    public void backButton(View view){
        onBackPressed();
    }

    public void dowFile(View view) {
        dowloadFile();
    }

    public void goFile(View view) {
            try {
                FileInputStream fis = openFileInput("file_name");
                BufferedReader r = new BufferedReader(new InputStreamReader(fis));

                file = r.readLine();
                r.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
    public void dowloadFile(){
        try {
            FileOutputStream fos = openFileOutput("file_name",Context.MODE_PRIVATE);
            Writer out = new OutputStreamWriter(fos);
            out.write(file);
            out.close();

            Toast toast = Toast.makeText(getApplicationContext(),
                    "Выполнено", Toast.LENGTH_SHORT);
            toast.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
