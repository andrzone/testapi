package com.example.android.playmarket;

import android.media.Image;
import android.widget.ImageView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.squareup.picasso.Picasso;

import java.sql.Time;
import java.text.SimpleDateFormat;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ^_^ on 19.03.2018.
 */

public class PostModel {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("time")
    @Expose
    private int time;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("file")
    @Expose
    private String file;
    @SerializedName("comment")
    @Expose
    private String comment;
    private ImageView image;

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getVersion() {

        return version;
    }

    public void setVersion(String version) {

        this.version = version;
    }

    public String getTime() {
        //Time dt = new Time (time * 1000);
        SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return sfd.format(time* 1000);
    }

    public void setTime(int time) {

        this.time = time;
    }

    public String getUrl() {

        return url;
    }

    public void setUrl(String url) {

        this.url = url;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {

        this.file = file;
    }

    public String getComment() {

        return comment;
    }

    public void setComment(String comment) {

        this.comment = comment;
    }



}
